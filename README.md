# vue3-movie-catalog

## Setup

- [Pinia](https://pinia.vuejs.org/) for saving and manipulating movies.
- [Axios](https://axios-http.com/) for api requests to the movies database.
- [Vueuse](https://vueuse.org/). Used useDebounceFn from this lib.

Preconfigured linter is here with typescript for type safe code.

## Time spent: 4h with 15 mins deploy
