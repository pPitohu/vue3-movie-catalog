import { useDebounceFn } from '@vueuse/core'
import { defineStore } from 'pinia'
import { ref, watch } from 'vue'
import { MOVIES_API } from './api'
import { Movie } from './types'

export const useMovies = defineStore('movies', () => {
  const movies = ref<Movie[]>([])
  const error = ref('')
  const currentPage = ref(1)
  const totalPages = ref(1)
  const totalMovies = ref(0)

  const lastSearchText = ref('')

  const searchMovies = async (searchText: string, page: number = currentPage.value) => {
    error.value = ''

    const {
      error: moviesError,
      movies: moviesList,
      totalMovies: totalMoviesAmount,
      totalPages: pages
    } = await MOVIES_API.searchMovies(searchText, page)
    
    if (moviesError) {
      error.value = moviesError
      movies.value = []
      currentPage.value = 1
      return
    }
    
    movies.value = moviesList
    totalPages.value = pages
    totalMovies.value = totalMoviesAmount
    lastSearchText.value = searchText
  }

  watch(currentPage, useDebounceFn(() =>searchMovies(lastSearchText.value), 300))

  return {
    movies,
    searchMovies,
    error,
    currentPage,
    totalPages,
    totalMovies
  }
})
