export interface Movie {
  Title: string,
  imdbID: string,
  Year: string,
  Type: string,
  Poster: string
}

export const MOVIE_PER_PAGE = 10
