import { instance } from '@/utils/axios'
import { Movie, MOVIE_PER_PAGE } from './types'

export const MOVIES_API = {
  searchMovies: async (searchText: string, page: number) => {
    const { data: movies } = await instance.get(import.meta.env.VITE_API_BASE_PARAMS, {
      params: { s: searchText, page }
    })
    const totalMovies = +movies.totalResults || 0
    let totalPages = 1
    if (totalMovies > 10) {
      totalPages = Math.ceil(totalMovies / MOVIE_PER_PAGE)
    }
    
    return {
      movies: movies.Search as Movie[],
      totalMovies,
      totalPages,
      error: movies.Error,
    }
  }
}
